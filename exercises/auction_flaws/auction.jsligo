type storage = { owner: address, deadline : timestamp, bids: map<address, tez>, topBidder: address, topBid: tez };
type action = | ["Bid"] | ["CollectTopBid"] | ["Claim"];

const bid = (s: storage) : storage => {
    if (Tezos.get_now () > s.deadline) {
        return failwith("Too late!") as storage;
    } else if (Map.find_opt(Tezos.get_sender (), s.bids) != (None() as option<tez>)) {
        return failwith("You already bid") as storage;
    } else {
        let newS = ({...s, bids: Map.update(Tezos.get_sender (), Some(Tezos.get_amount ()), s.bids)});
        if (newS.topBid < Tezos.get_amount ()) {
            return ({...newS, topBid: Tezos.get_amount (), topBidder: Tezos.get_sender ()});
        } else {
            return newS;
        }
    }
};

const collectTopBid = (s: storage) : storage => {
    if (Tezos.get_now () < s.deadline) {
        return failwith("Too early!") as storage;
    } else if (Tezos.get_sender () != s.owner) {
        return failwith("Not the owner");
    } else {
        let receiverOpt = Tezos.get_contract_opt (s.owner) as option<contract<unit>>;
        let receiver : contract<unit> = match (receiverOpt, {
            Some : (contract : any) => contract,
            None : () => failwith("Contract not found") as contract<unit>
        });
        let _op = Tezos.transaction(unit, s.topBid, receiver);
        return s;
    }
};

const claim = (s: storage) : storage => {
    if (Tezos.get_now () < s.deadline) {
        return failwith("Too early!") as storage;
    } else if (Tezos.get_sender () == s.topBidder) {
        return failwith("You won the auction") as storage;
    } else {
        let receiverOpt = Tezos.get_contract_opt (Tezos.get_sender ()) as option<contract<unit>>;
        let receiver : contract<unit> = match (receiverOpt, {
            Some : (contract : any) => contract,
            None : () => failwith("Contract not found") as contract<unit>
        });
        let bid = match(Map.find_opt (Tezos.get_sender (), s.bids), {
            Some: (bid: tez) => bid,
            None: () => (failwith("You have not bid") as tez)
        });
        let _op = Tezos.transaction(unit, bid, receiver);
        return s;
    }
};

const main = ([p, s] : [action, storage]) : [list<operation>, storage] => {
   let stor = match(p, {
      Bid : () => bid(s),
      CollectTopBid : () => collectTopBid(s),
      Claim : () => claim(s)
   });
   return [list([]) as list<operation>, stor];
};
